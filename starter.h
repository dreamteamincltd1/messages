#ifndef STARTER_H
#define STARTER_H
#include <QObject>
#include "core.h"
#include "widget.h"
#include "wizard/configwizard.h"
#include "configuration/configurationreader.h"
#include "configuration/parameters.h"
#include "dialer/dialercore.h"
#include "dialer/dialermanager.h"

class Starter : public QWidget {
    Q_OBJECT
private:
    QFile* config_file;
    Core* core;
    Widget* gui;
    DialerCore* dialer_core;
    DialerManager* dialer_manager;
    ConfigWizard* wizard;
    Parameters parameters;
    Parameters readParameters();
    void runWizard();
public slots:
    void startApplication();
public:
    Starter();
    ~Starter();
};

#endif // STARTER_H
