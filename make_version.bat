set PATH=%PATH%;D:\Apps\Qt\Tools\mingw482_32\bin;C:\Program Files (x86)\WiX Toolset v3.9\bin
::set PATH=%PATH%;C:\Qt\Qt_5_3\Tools\mingw482_32\bin;C:\Program Files (x86)\WiX Toolset v3.9\bin
set EXENAME=local_chatv4

git pull
rd /q /s build
mkdir build
increase.exe
git commit version.txt -m "increase version"
git push
copy installer.wxs build\installer.wxs
qmake -makefile -o build\Makefile
cd build
mingw32-make clean
mingw32-make 
mkdir files
copy release\%EXENAME%.exe files\
copy ..\version.txt files\
windeployqt --release files\%EXENAME%.exe
candle -fips installer.wxs
light -ext WixUIExtension installer.wixobj