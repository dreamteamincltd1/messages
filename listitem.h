#ifndef LISTITEM_H
#define LISTITEM_H

#include <QListWidgetItem>
#include "userprofile.h"

class ListItem : public QListWidgetItem
{
public:
    ListItem(UserProfile * prof);
    QString getUserID() {return m_userID;}
    QString getName()   {return m_name;}
    QString getSubname(){return m_subname;}
    UserProfile* getProfile() {return m_profile;}
    QString text() const{
        return m_name + " " + m_subname;
    }
signals:

public slots:

private:
    QString m_name;
    QString m_subname;
    QString m_userID;
    UserProfile* m_profile;
};

#endif // LISTITEM_H
