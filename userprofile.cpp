#include "userprofile.h"
#include <QHostAddress>
#include <QTcpSocket>

UserProfile::UserProfile(QString name, QString subname, QUuid uuid, QTcpSocket *s, bool online){
    m_name = name;
    m_uuid = uuid;
    m_subname = subname;
    m_sock = s;
    m_online = online;
}

UserProfile::UserProfile(QString str){
    m_uuid = QUuid::createUuid();
    m_name = str;
}

void UserProfile::setSocket(QTcpSocket *s){
    m_sock = s;
}

void UserProfile::setName(QString name){
    m_name = name;
}

void UserProfile::setOnline(bool b){
    m_online = b;
}

QTcpSocket *UserProfile::getSocket(){
    return m_sock;
}

QString UserProfile::getUserID(){
    return m_uuid.toString();
}

QString UserProfile::getName(){
    return m_name;
}

QString UserProfile::getSubname(){
    return m_subname;
}

bool UserProfile::isOnline(){
    return m_online;
}

QString UserProfile::debug(){
    return QString("Name:%1\nSubname:%2\nUUID:%3\nIP:%4\nportTcp:%5").arg(m_name, m_subname, m_uuid.toString(),
                                                                          m_sock->peerAddress().toString(),QString::number(m_sock->peerPort()));
}

void UserProfile::addToHistory(QString str){
    m_history.append(str + "\n");
}

QString UserProfile::getHistory(){
    return m_history;
}

void UserProfile::setHistory(QString str){
    m_history = str;
}
