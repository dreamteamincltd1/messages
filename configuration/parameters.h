#ifndef PARAMETERS_H
#define PARAMETERS_H
#include <QString>
#include <QUuid>
#include <QNetworkConfiguration>

class Parameters {
private:
    QString name;
    QString subname;
    QUuid uuid;
    QNetworkConfiguration network_configuration;
    QString IPv4;
    int portTcp;
    int portUdp;
public:
    Parameters(QString name, QString subname, QUuid uui, QNetworkConfiguration net_cfg);
    Parameters();
    bool isValid();
    QString debug();
    QString getIPv4();
    QString getName(){
        return name;
    }
    QString getSubname(){
        return subname;
    }
    QUuid getUuid(){
        return uuid;
    }
    int getPortTcp(){
        return portTcp;
    }
    int getPortUdp(){
        return portUdp;
    }
};

#endif // PARAMETERS_H
