#include "configurationreader.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QNetworkConfigurationManager>
#include <QNetworkConfiguration>
#include <QNetworkSession>
#include <QIODevice>

Parameters ConfigurationReader::read(QFile &file){
    if(!file.exists())
        return Parameters();
    QJsonParseError parse_error;
    file.open(QIODevice::ReadWrite);
    QJsonDocument document = QJsonDocument::fromJson(file.readAll(), &parse_error);
    file.close();
    if(parse_error.error != QJsonParseError::NoError)
        return Parameters();
    QJsonObject object = document.object();
    QString name = object.take("name").toString();
    QString subname = object.take("subname").toString();
    QUuid uuid = QUuid(object.take("uuid").toString());
    QString network_config_id = object.take("network_configuration_id").toString();
    QNetworkConfigurationManager manager;
    QNetworkConfiguration config = manager.configurationFromIdentifier(network_config_id);
    return Parameters(name, subname, uuid, config);

}
