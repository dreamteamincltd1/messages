#include "parameters.h"
#include <QNetworkSession>
#include <QNetworkInterface>
#include <QNetworkAddressEntry>
#include <QAbstractSocket>

QString calcIPv4(const QNetworkConfiguration& configuration){
    QNetworkSession session(configuration);
    QNetworkInterface interface = session.interface();
    QList<QNetworkAddressEntry> addresses = interface.addressEntries();
    for(QList<QNetworkAddressEntry>::const_iterator address = addresses.begin(); address != addresses.end(); ++address){
        if(address->ip().protocol() == QAbstractSocket::IPv4Protocol){
            return address->ip().toString();
        }
    }
    return "IPv4 NOT FOUND IN THIS CONNECTION";
}


Parameters::Parameters(){
    uuid = QUuid("{00000000-0000-0000-0000-000000000000}");
}

Parameters::Parameters(QString name, QString subname, QUuid uuid, QNetworkConfiguration net_cfg){
    this->name = name;
    this->subname = subname;
    this->uuid = uuid;
    this->network_configuration = net_cfg;
    this->IPv4 = calcIPv4(net_cfg);
    portTcp = 56011;
    portUdp = 56012;
}

bool Parameters::isValid(){
    return uuid.toString() != "{00000000-0000-0000-0000-000000000000}";
}

QString Parameters::debug(){
    return QString("Name:%1\nSubname:%2\nUUID:%3\nNetwork:%4\nIP:%5\nportTcp:%6\nportUdp:%7").arg(name, subname, uuid.toString(), network_configuration.name(), IPv4,
                                                                                                  QString::number(portTcp),QString::number(portUdp));
}

QString Parameters::getIPv4(){
    return IPv4;
}
