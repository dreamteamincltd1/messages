#ifndef CONFIGURATIONREADER_H
#define CONFIGURATIONREADER_H
#include "Parameters.h"
#include <QFile>
class ConfigurationReader {
public:
    static Parameters read(QFile& file);
};

#endif // CONFIGURATIONREADER_H
