#include "Starter.h"
#include "QApplication"
#include <QStandardPaths>
#include <QDir>
#include "stdio.h"
#include <string>


void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg){
    QString appdata_path = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    QFile log_file(appdata_path + "/log.txt");
    log_file.open(QFile::Append);
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        log_file.write(localMsg+"\n");
        fprintf(stdout, "%s \n", localMsg.constData());
        break;
    case QtWarningMsg:
        log_file.write(localMsg+"\n");
        fprintf(stdout, "Warning: %s \n", localMsg.constData());
        break;
    case QtCriticalMsg:
        log_file.write(localMsg+"\n");
        fprintf(stdout, "Critical: %s \n", localMsg.constData());
        break;
    case QtFatalMsg:
        log_file.write(localMsg+"\n");
        fprintf(stdout, "Fatal: %s \n", localMsg.constData());
        abort();
    }
    log_file.close();
}

int main(int argc, char **argv){
    QString appdata_path = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    QDir::current().mkpath(appdata_path);
    qInstallMessageHandler(myMessageOutput);
    QApplication a(argc, argv);
    Starter* st = new Starter();
    int ret = a.exec();
    delete st;
    return ret;
}
