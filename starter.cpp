#include "starter.h"
#include <QDir>
#include <QStandardPaths>
#include "userprofile.h"
#include <QUuid>

Starter::Starter(){
    QString appdata_path = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    QDir::current().mkpath(appdata_path);
    QString config_filename = appdata_path + "/BUTbKO.conf";
    config_file = new QFile(config_filename);
    wizard = new ConfigWizard(config_file);
    connect(wizard, SIGNAL(accepted()), this, SLOT(startApplication()));
    if(config_file->exists()){
        parameters = ConfigurationReader::read(*config_file);
        startApplication();
    }else{
        wizard->show();
    }
}

Starter::~Starter(){
    delete wizard;
    delete gui;
    delete core;
    delete dialer_manager;
    delete dialer_core;
}

void Starter::startApplication(){
    if(!parameters.isValid())
        parameters = ConfigurationReader::read(*config_file);
    core = new Core(parameters);
    gui = new Widget;
    gui->show();
    dialer_manager = new DialerManager();
    dialer_core = new DialerCore(parameters);

    //  КОННЕКТЫ ДЛЯ ЗВУКОВОГО МОДУЛЯ
    connect(dialer_manager, SIGNAL(startTalk(UserProfile*)), dialer_core, SLOT(callingFrom(UserProfile*)));
    connect(dialer_manager, SIGNAL(stopTalk()), dialer_core, SLOT(endCall()));
    connect(dialer_manager, SIGNAL(serviceMessage(QUuid,QList<QPair<QString,QString> >)), core, SLOT(slotSendServiceMessage(QUuid,QList<QPair<QString,QString> >)));
    connect(core, SIGNAL(signalIncomingCall(UserProfile*)), dialer_manager, SLOT(oCall(UserProfile*)));
    connect(core, SIGNAL(signalCallAccept(UserProfile*)), dialer_manager, SLOT(oAccept(UserProfile*)));
    connect(core, SIGNAL(signalCallReject(UserProfile*)), dialer_manager, SLOT(oReject(UserProfile*)));
    connect(core, SIGNAL(signalCallEnd(UserProfile*)), dialer_manager, SLOT(oEnd(UserProfile*)));
    connect(gui, SIGNAL(signalCallTo(UserProfile*)), dialer_manager, SLOT(call(UserProfile*)));

    //  КОНЕЦ

    /*
    connect(gui,SIGNAL(signalBindToUdp(QHostAddress,int)),
            core,SLOT(slotBindToUdp(QHostAddress,int)));
    */
    connect(gui,SIGNAL(signalConnectToTcp(QHostAddress,int)),
            core,SLOT(slotConnectToTcp(QHostAddress,int)));
    connect(gui,SIGNAL(signalListenTcp(QHostAddress,int)),
            core,SLOT(slotListenTcp(QHostAddress,int)));
    connect(gui,SIGNAL(signalSendIOnlineMessage(int)),
            core,SLOT(slotSendIOnlineMessage(int)));

    connect(gui,SIGNAL(signalPrintUserProfileList()),
            core,SLOT(slotPrintUserProfileList()));

    connect(gui,SIGNAL(signalBindUdp(QHostAddress,int)),
            core,SLOT(slotBindUdp(QHostAddress,int)));
    connect(core,SIGNAL(signalUpdateListContacts(QList<UserProfile*>)),
            gui,SLOT(slotUpdateListContact(QList<UserProfile*>)));
    connect(core,SIGNAL(signalNewUserMessage(UserProfile*,QString)),
            gui,SLOT(slotNewUserMessage(UserProfile*,QString)));
    connect(gui,SIGNAL(signalSendServiceMessage(QUuid,QList<QPair<QString,QString> >)),
            core,SLOT(slotSendServiceMessage(QUuid,QList<QPair<QString,QString> >)));
}

void Starter::runWizard(){
}
