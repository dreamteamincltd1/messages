#include "widget.h"
#include <QGridLayout>
#include "userprofile.h"
#include "listitem.h"
#include <QList>
#include <QPair>

Widget::Widget(){
    setWindowIcon(QIcon(":/call_icon"));
    qDebug()<<"Widget::Widget(): create Main Window";
    m_listContact = new QListWidget(this);
    m_textIn = new QTextEdit(this);
    m_textOut = new QTextEdit(this);
    m_lineListenAddr = new QLineEdit("127.0.0.1");
    m_lineListenPort = new QLineEdit("56011");
    m_lineConnectAddr = new QLineEdit("127.0.0.1");
    m_lineConnectPort = new QLineEdit("23777");
    m_lineBindPort = new QLineEdit("56012");
    m_butSendMessage = new QPushButton("Send");
    m_butListen = new QPushButton("Listen");
    m_butConnect = new QPushButton("Connect");
    m_butBind = new QPushButton("Bind");
    m_butIOnline = new QPushButton("IOnline");
    m_butCall = new QPushButton("Call");

    QHBoxLayout * main = new QHBoxLayout();
    QVBoxLayout * mainLeft = new QVBoxLayout();
    QVBoxLayout * mainRight = new QVBoxLayout();
    QGridLayout * mainLeftDown = new QGridLayout();
    QVBoxLayout * mainRightDown = new QVBoxLayout();

    main->addLayout(mainLeft);
    main->addLayout(mainRight);
    mainLeft->addWidget(m_listContact);
    /*
     * Убрать кнопки отладки
     * */
    //mainLeft->addLayout(mainLeftDown);

    mainLeftDown->addWidget(m_lineListenAddr,1,1);
    mainLeftDown->addWidget(m_lineListenPort,1,2);
    mainLeftDown->addWidget(m_butListen,1,3);
    mainLeftDown->addWidget(m_lineConnectAddr,2,1);
    mainLeftDown->addWidget(m_lineConnectPort,2,2);
    mainLeftDown->addWidget(m_butConnect,2,3);
    mainLeftDown->addWidget(m_lineBindPort,3,2);
    mainLeftDown->addWidget(m_butBind,3,3);
    mainLeftDown->addWidget(m_butIOnline,4,3);

    mainRight->addWidget(m_textIn);
    mainRight->addLayout(mainRightDown);
    mainRightDown->addWidget(m_textOut);
    mainRightDown->addWidget(m_butSendMessage);
    mainRightDown->addWidget(m_butCall);

    setLayout(main);


    //Buttons
    connect(m_butListen,SIGNAL(clicked()),SLOT(slotButListenClicked()));
    connect(m_butConnect,SIGNAL(clicked()),SLOT(slotButConnectClicked()));
    connect(m_butBind,SIGNAL(clicked()),SLOT(slotButBindClicked()));
    connect(m_butIOnline,SIGNAL(clicked()),SLOT(slotButIOnlineClicked()));
    connect(m_butSendMessage,SIGNAL(clicked()),SLOT(slotButSendClicked()));
    connect(m_butCall, SIGNAL(clicked()), SLOT(slotCall()));

    //Core
    /*
    m_core = core;
    connect(m_core,SIGNAL(signalNewUserMessage(QString)),this,SLOT(slotNewUserMessage(QString)));
    connect(m_core,SIGNAL(signalNewConnection(UserProfile*)),SLOT(slotNewConnection(UserProfile*)));
    connect(m_core,SIGNAL(signalDisconnectContact(UserProfile*)),SLOT(slotDisconnectContact(UserProfile*)));
*/
    connect(m_listContact,SIGNAL(clicked(QModelIndex)),SLOT(slotListContactClicked(QModelIndex)));
    m_currID = 0;
    m_currProfile = NULL;
}

void parseAddrString(QString str,QHostAddress &haddr,int &port){
    QStringList list = str.split(':');
    if(list.size() == 2){
        haddr = list[0];
        port = list[1].toInt();
        qDebug()<<"parseAddrString() ok";
    }
    qDebug()<<list;
}
//+
void Widget::slotButListenClicked(){
    qDebug() << "Widget::slotButListenClicked()";
    QHostAddress addr(m_lineListenAddr->text());
    int port = m_lineListenPort->text().toInt();
    emit signalListenTcp(addr,port);
}
//+
void Widget::slotButConnectClicked(){
    qDebug() << "Widget::slotButConnectClicked() -> emit signalConnectToTcp(addr,port);";
    QHostAddress addr(m_lineConnectAddr->text());
    int port = m_lineConnectPort->text().toInt();
    emit signalConnectToTcp(addr,port);
}
//+
void Widget::slotButBindClicked(){
    qDebug() << "Widget::slotButBindClicked()";
    int port = m_lineBindPort->text().toInt();
    emit signalBindUdp(QHostAddress::LocalHost,port);
}
//+
void Widget::slotButIOnlineClicked(){
    qDebug() << "IOnline: emit signalPrintUserProfileList";
    emit signalPrintUserProfileList();
    /*** работает
    qDebug() << "Widget::slotButIOnlineClicked()";
    int port = m_lineBindPort->text().toInt();
    emit signalSendIOnlineMessage(port);
    */
}
//+
void Widget::slotButSendClicked(){
    qDebug()<<"Widget::slotButSendClicked()";
    if(m_currID.isNull()){
        qDebug() << "Widget: m_currID is NULL";
        return;
    }
    QString str = m_textOut->toPlainText();
    m_textOut->clear();
    QList<QPair<QString,QString> > list;
    list.append(QPair<QString,QString>("cmd","msg"));
    list.append(QPair<QString,QString>("msg",str));
    //emit signalSendServiceMessage(m_currID,"msg",str);
    m_textIn->append(">>" + str);
    emit signalSendServiceMessage(m_currID,list);
}
//+
void Widget::slotListContactClicked(QModelIndex i){
    qDebug()<<"Widget::slotListContactClicked()";
    ListItem * item = (ListItem *) i.internalPointer();
    m_currID = item->getUserID();
    m_currProfile = item->getProfile();
    m_textIn->clear();
    m_textIn->append(m_currProfile->getHistory());
    qDebug() << "Widget: m_currID = "<<m_currID;
}

void Widget::slotNewConnection(UserProfile *prof){
    qDebug()<<"Widget::slotNewConnection(UserProfile *prof)";
    //updateListContacts();
}

void Widget::slotDisconnectContact(UserProfile *prof){
    qDebug()<<"Widget::slotDisconnectContact(UserProfile *)"<<prof->getName();
    //updateListContacts();
}
//+
void Widget::slotUpdateListContact(QList<UserProfile *> list){
    qDebug() << "Widget::slotUpdateListContact: list size = " << list.size();
    m_listContact->clear();
    foreach (UserProfile * p, list) {
        //qDebug() << "Widget: " + p->debug();
        QListWidgetItem * it = new ListItem(p);
        m_listContact->addItem(it);
        qDebug() << "Widget: contact added";
    }
}

void Widget::slotNewUserMessage(UserProfile * prof, QString msg){
    qDebug() << "Widget::slotNewUserMessage(UserProfile * prof, QString msg)";
    QString prep;
    if (prof != 0){
        prep = prof->getName();
        //prep.append(prof->getSocket()->peerAddress().toString() + " ");
        //prep.append(QString::number((prof->getSocket()->peerPort())));
    }else{
        qDebug() << "Widget::slotNewUserMessage(): UserProfile is null";
    }
    m_textIn->append(prep + ": "+ msg);
}

Widget::~Widget(){
    delete m_lineListenAddr;
    delete m_lineListenPort;
    delete m_lineConnectAddr;
    delete m_lineConnectPort;
    delete m_lineBindPort;
    delete m_butSendMessage;
    delete m_butListen;
    delete m_butConnect;
    delete m_butBind;
    delete m_butIOnline;
    delete m_butCall;
}

void Widget::addItem(UserProfile *prof){
    QListWidgetItem * it = new ListItem(prof);
    m_listContact->addItem(it);
    /***/
}

void Widget::slotCall(){
    emit signalCallTo(m_currProfile);
}
