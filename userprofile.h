#ifndef USERPROFILE_H
#define USERPROFILE_H
#include <QTcpSocket>
#include <QUuid>

class UserProfile
{
public:
    UserProfile(QString name, QString subname, QUuid uuid, QTcpSocket * s, bool online = true);
    UserProfile(QString str);
    void setSocket(QTcpSocket * s);
    void setName(QString name);
    void setOnline(bool b);
    QTcpSocket *    getSocket();
    QString         getUserID();
    QString         getName();
    QString         getSubname();
    bool            isOnline();
    QString         debug();
    void addToHistory(QString str);
    QString getHistory();
    void setHistory(QString str);
private:
    QTcpSocket * m_sock;
    QUuid m_uuid;
    QString m_name;
    QString m_subname;
    bool m_online;
    QString m_history;
};

#endif // USERPROFILE_H
