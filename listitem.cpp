#include "listitem.h"

ListItem::ListItem(UserProfile * prof)
    :QListWidgetItem(prof->getName() + " " + prof->getSubname())
{
    QString text = ((QListWidgetItem *)this)->text();
    if(prof->isOnline()){
        this->setText(text + "\t В СЕТИ");
    }else{
        this->setText(text + "\t НЕ В СЕТИ");
    }
    m_userID = prof->getUserID();
    m_name = prof->getName();
    m_subname = prof->getSubname();
    m_profile = prof;
}
