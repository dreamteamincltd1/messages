#ifndef CORE_H
#define CORE_H

#include <QObject>
#include <QMainWindow>
#include <QHostAddress>
#include <QTcpServer>
#include <QTcpSocket>
#include <QList>
#include <QString>
#include <QModelIndex>
#include <QUdpSocket>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include "userprofile.h"
#include <QUuid>
#include "configuration/Parameters.h"


class Core : public QObject
{
    Q_OBJECT

public:
    explicit Core(Parameters parameters, QObject *parent = 0);

signals:
    void signalNewConnection(UserProfile * prof);
    void signalDisconnectContact(UserProfile * prof);
    void signalNewUserMessage(UserProfile* prof, QString msg);
    void signalUpdateListContacts(QList<UserProfile*> prof);
    void signalIncomingCall(UserProfile* profile);
    void signalCallAccept(UserProfile* profile);
    void signalCallReject(UserProfile* profile);
    void signalCallEnd(UserProfile* profile);
public slots:

    //void slotReadTcpMessage();
    void slotDisconnectTcp();
    void slotNewConnectionTcp();
    void slotReadUdpMessage();
    //НОВОЕ
    /*!!!!!!*/void slotConnectToTcp(QHostAddress addr, int port);
    void slotGetInfo();
    void slotListenTcp(QHostAddress addr,int port);
    void slotBindUdp(QHostAddress addr,int port);
    void slotSendIOnlineMessage(int port);
    void slotSendServiceMessage(QUuid userID, QList<QPair<QString,QString> > commandList);
    void slotSendServiceMessage(QTcpSocket *sock, QString cmd);
    void slotReadServiceMessage();
    void slotAddUserProfile(UserProfile *prof);
    void slotPrintUserProfileList();
    //НОВОЕ

private:
    QTcpServer * m_tcpServer;
    QUdpSocket * m_udpSocket;
    Parameters m_parameters;                    //Параметры личного профиля
    QList<UserProfile *>m_listUserProfile;      //Лист сокетов, с именами, userID и сокетами
    QList<QTcpSocket *>m_listTmpSockets;        //Лист сокетов, ожидающих соединения, и пока не имеющие userID

    int getIndexInProfileList(QUuid id);
    int getIndexInProfileList(QHostAddress addr,int port);
    void removeSockFromListTmp(QTcpSocket * s);
};

#endif // CORE_H
