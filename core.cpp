#include "core.h"
#include <QJsonObject>
#include <QString>
#include <QPair>
#include <QList>

Core::Core(Parameters parameters, QObject *parent) :
    QObject(parent)
{
    m_tcpServer = new QTcpServer(this);
    m_udpSocket = new QUdpSocket(this);
    m_parameters = parameters;
    qDebug() << "Core running with parameters: " << endl << parameters.debug();

    QHostAddress addr(m_parameters.getIPv4());
    slotListenTcp(addr,m_parameters.getPortTcp());

    slotBindUdp(QHostAddress(m_parameters.getIPv4()),parameters.getPortUdp());
    slotSendIOnlineMessage(m_parameters.getPortUdp());
}

//+
void Core::slotDisconnectTcp(){
    qDebug() << "Core::slotDisconnectTcp()";
    QTcpSocket * s = (QTcpSocket *)sender();
    int ind = getIndexInProfileList(s->peerAddress(),s->peerPort());
    if(ind!=-1){
        //m_listUserProfile.removeAt(ind);
        m_listUserProfile[ind]->setOnline(false);
        emit signalUpdateListContacts(m_listUserProfile);
    }
}

//+
void Core::slotListenTcp(QHostAddress addr, int port){
    qDebug()<<"Core::slotListenTcp()"<<addr<<" "<<port;
    m_tcpServer->listen(addr,port);
    connect(m_tcpServer,SIGNAL(newConnection()),SLOT(slotNewConnectionTcp()));
}

void Core::slotBindUdp(QHostAddress addr, int port){
    qDebug()<<"Core::slotBindToUdp()";
    //qDebug() << addr << port;
    bool ret = m_udpSocket->bind(addr,port);
    qDebug() << "Core::slotBindToUdp(). ret = " << ret;
    connect(m_udpSocket,SIGNAL(readyRead()),SLOT(slotReadUdpMessage()));
}

void Core::slotSendIOnlineMessage(int port){
    qDebug()<<"Core::sendIOnlineMessage()";
    QByteArray datagram("I online");
    int ret = m_udpSocket->writeDatagram(datagram,QHostAddress::Broadcast,port);
    qDebug()<<"Core::sendIOnlineMessage().write "<<ret<<" bytes";
    if(ret == -1){
        qDebug()<<"Core::sendIOnlineMessage(): error writing";
    }
}

void Core::slotConnectToTcp(QHostAddress addr, int port){
    qDebug() << "Core::slotConnectToTcp(QHostAddress addr, int port)" << addr <<port;
    int ind = getIndexInProfileList(addr,port);
    if(ind != -1){
        qDebug() << "Core: connection already establish";
        return;
    }
    QTcpSocket * sock = new QTcpSocket(this);
    connect(sock,SIGNAL(connected()),SLOT(slotGetInfo()));
    connect(sock,SIGNAL(readyRead()),SLOT(slotReadServiceMessage()));
    m_listTmpSockets.append(sock);
    sock->connectToHost(addr,port);
}

void Core::slotGetInfo(){
    qDebug() << "Core::slotSendInfo()";
    QTcpSocket * sock = (QTcpSocket*)sender();
    qDebug() << "Core: connection establish" << sock->peerAddress() << sock->peerPort();

    QJsonObject obj;
    obj["cmd"] = QJsonValue(QString("info"));
    QJsonDocument doc(obj);
    int ret = sock->write(doc.toJson());
    qDebug() << "Core: write " << ret << "bytes";
}
//+
void Core::slotNewConnectionTcp(){
    qDebug()<<"Core::slotNewConnectionTcp()";
    while(m_tcpServer->hasPendingConnections()){
            QTcpSocket * sock = m_tcpServer->nextPendingConnection();
            connect(sock,SIGNAL(readyRead()),SLOT(slotReadServiceMessage()));
            connect(sock,SIGNAL(disconnected()),SLOT(slotDisconnectTcp()));
            slotSendServiceMessage(sock,"info");
            m_listTmpSockets.append(sock);
        }
}

//+
void Core::slotSendServiceMessage(QUuid userID, QList<QPair<QString,QString> > commandList){
    qDebug() << "Core::slotSendServiceMessage(QUuid userID, QList<QPair<QString,QString> > )";
    int ind = getIndexInProfileList(userID);
    if(ind == -1){
        qDebug() << "Core: userID is not found";
        return;
    }
    QTcpSocket * sock = m_listUserProfile[ind]->getSocket();
    QJsonObject obj;
    for(int i = 0; i < commandList.size(); i++){
        QString &name = commandList[i].first;
        QString &body = commandList[i].second;
        obj[name] = QJsonValue(body);
    }
    QJsonDocument doc(obj);
    int ret = sock->write(doc.toJson());

    if(obj["cmd"] == "msg"){
        if(ind == -1)qDebug()<<"Core::slotSendServiceMessage(): ind == -1 ERROR WRITING HISTORY";
        m_listUserProfile[ind]->addToHistory(">>" + obj["msg"].toString());
    }

    if(ret != -1){
        qDebug()<<"Core::slotSendServiceMessage(): writing "<<ret<<" bytes";
    }else{
        qDebug()<<"Core::slotSendServiceMessage(): error writing. ret = "<<ret;
    }
}

//+
void Core::slotSendServiceMessage(QTcpSocket * sock, QString cmd){
    QJsonObject obj;
    obj["cmd"] = QJsonValue(QString(cmd));
    QJsonDocument doc(obj);
    int ret = sock->write(doc.toJson());
    qDebug() << "Core: write " << ret << "bytes";
}
//+
QList<QByteArray> parseMessage(QByteArray &arr){
    QString str(arr);
    QList<QByteArray> data;
    QString nstr;
    int begin = 0;
    int end = 0;
    int count = 0;
    for(int i = 0; i < str.size(); i++){
        if(str[i] == '{'){
            count++;
        }else if(str[i] == '}'){
            count--;
        }
        if(count == 0){
            end = i;
            QString nstr;
            for(int i=begin; i<=end; i++){
                nstr.append(str[i]);
            }
            QByteArray a;
            a.append(nstr);
            data.append(a);
            begin = i+1;
        }
    }
    return data;
}

//+
void Core::slotReadServiceMessage(){
    qDebug() << "Core::slotReadServiceMessage";
    QTcpSocket * sock = (QTcpSocket *) sender();
    QByteArray array = sock->readAll();
    qDebug() << "Core::slotReadServiceMessage(): read = " << array;
    QHostAddress addr = sock->peerAddress();
    quint16 port = sock->peerPort();
    UserProfile* sender_profile = NULL;
    int index = getIndexInProfileList(addr, port);
    qDebug() << addr << port;
    qDebug() << index;
    if(index >= 0){
        sender_profile = m_listUserProfile[index];
    }

    QList<QByteArray> command = parseMessage(array);
    qDebug() << "Core: command size = " << command.size();
    for(int i=0;i<command.size();i++){

        QByteArray array = command[i];

        QJsonDocument doc(QJsonDocument::fromJson(array));
        QString cmd = doc.object()["cmd"].toString();
        if (cmd == "msg"){
            QString msg = doc.object()["msg"].toString();
            sender_profile->addToHistory(sender_profile->getName() + ": " + msg);
            emit signalNewUserMessage(sender_profile, msg);
        }else if(cmd == "info"){
            QJsonObject obj;
            obj["cmd"] = QJsonValue(QString("info_answer"));
            obj["name"] = QJsonValue(m_parameters.getName());
            obj["subname"] = QJsonValue(m_parameters.getSubname());
            obj["uuid"] = QJsonValue(m_parameters.getUuid().toString());
            QJsonDocument doc(obj);
            int ret = sock->write(doc.toJson());
            if(ret != -1){
                qDebug()<<"Core::sendInfoAnswer(): writing "<<ret<<" bytes";
            }else{
                qDebug()<<"Core::sendInfoAnswer(): error writing. ret = "<<ret;
            }
        }else if(cmd == "info_answer"){
            //Удалить из очереди ожидания
            //Добавить соответствующий userprofile
            QString name = doc.object()["name"].toString();
            QString subname = doc.object()["subname"].toString();
            QString suuid = doc.object()["uuid"].toString();
            QUuid uuid(suuid);
            UserProfile * prof = new UserProfile(name,subname,uuid,sock,true);
            removeSockFromListTmp(sock);
            slotAddUserProfile(prof);
        }else if(cmd == "call"){
            if(sender_profile == NULL){
                qDebug() << "SENDER PROFILE IS NULL";
            }else{
                emit signalIncomingCall(sender_profile);
            }
        }else if(cmd == "call_answer"){
            if(sender_profile == NULL){
                qDebug() << "SENDER PROFILE IS NULL";
            }else{
                QString arg = doc.object()["call"].toString();
                if(arg == "accept"){
                    emit signalCallAccept(sender_profile);
                }else if(arg == "reject"){
                    emit signalCallReject(sender_profile);
                }else if(arg == "end"){
                    emit signalCallEnd(sender_profile);
                }else{
                    qDebug() << "Incorrent call arg: " << arg;
                }
            }
        }

    }
}
//+
void Core::slotAddUserProfile(UserProfile * prof){
    qDebug() << "Core::slotAddUserProfile: " << prof->getName();
    //свой профиль не добавляем.
    if(prof->getUserID() == m_parameters.getUuid().toString()){
        qDebug() << "Core::slotAddUserProfile: do not add self profile";
        return;
    }
    int ind = getIndexInProfileList(prof->getUserID());
    if(ind != -1){
        //Найден профиль, значит нужно обносить его.
        /*
        qDebug() << "User already exist in listUserProfile, deleting newest prof";
        delete prof;
        return;
        */
        qDebug() << "User already exist in listUserProfile. Update profile";
        m_listUserProfile.at(ind)->setOnline(true);
        m_listUserProfile.at(ind)->setSocket(prof->getSocket());
        emit signalUpdateListContacts(m_listUserProfile);
        return;
    }
    m_listUserProfile.append(prof);
    qDebug() << "Core: emint signalUpdateListContacts";
    emit signalUpdateListContacts(m_listUserProfile);
}

void Core::slotPrintUserProfileList(){
    qDebug() << "Core::slotPrintUserProfileList(): size = " << m_listUserProfile.size();
    for(int i=0;i<m_listUserProfile.size();i++){
        qDebug() << m_listUserProfile[i]->debug();
    }
}
//+
int Core::getIndexInProfileList(QUuid id){
    for(int i=0;i<m_listUserProfile.size();i++){
        if(m_listUserProfile[i]->getUserID() == id.toString()){
            return i;
        }
    }
    return -1;
}
//+
int Core::getIndexInProfileList(QHostAddress addr, int port){
    for(int i=0;i<m_listUserProfile.size(); ++i){
        QHostAddress a = m_listUserProfile[i]->getSocket()->peerAddress();
        int p = m_listUserProfile[i]->getSocket()->peerPort();
        //*** Может надо комментить порт
        if(a == addr /*&& p == port*/){
            return i;
        }
    }
    return -1;
}
//+
void Core::removeSockFromListTmp(QTcpSocket *s){
    int j = 0;
    for(int i=0;i<m_listTmpSockets.size();i++){
        if(s == m_listTmpSockets[i]){
            j = i;
            break;
        }
    }
    m_listTmpSockets.removeAt(j);
    qDebug() << "Core::removeSockFromListTmp: " << j << "number";
}

//+
void Core::slotReadUdpMessage(){
    qDebug()<<"Core::slotReadUdpMessage()";
    QByteArray datagram;
    datagram.resize(m_udpSocket->pendingDatagramSize());
    QHostAddress sender;
    quint16 senderPort;
    m_udpSocket->readDatagram(datagram.data(),datagram.size(),&sender,&senderPort);
    QByteArray arr("I online");
    if(datagram == arr){
        qDebug()<<"Core::slotReadUdpMessage().readBroadCastDatagram. read this is online words";
        //sendera нет в листе -> добавить и активировать соединение
        //initTcpSocketAndConnection(sender,senderPort);
        slotConnectToTcp(sender,m_parameters.getPortTcp());
    }else{
        qDebug()<<"Core::slotReadUdpMessage(). message format not recognized. msg = "
               <<datagram;
    }
}

/*
 * Занести в служебный протокол данные о команде getUID
 * */
