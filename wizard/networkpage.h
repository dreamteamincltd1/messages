#ifndef NETWORKPAGE_H
#define NETWORKPAGE_H
#include <QWizardPage>
#include <QLabel>
#include <QGridLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QNetworkConfiguration>
class NetworkPage : public QWizardPage {
    Q_OBJECT
private:
    QLabel* network_label;
    QComboBox* network_edit;
    QGridLayout* layout;
    QLineEdit* ip_address_show;
    QLineEdit* identifier_show;
    QMap<QString, QNetworkConfiguration> network_configurations;
private slots:
    void networkConfigurationChanged(QString string);
public:
    NetworkPage(QWidget* parent = 0);
};

#endif // NETWORKPAGE_H
