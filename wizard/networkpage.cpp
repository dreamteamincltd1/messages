#include "networkpage.h"
#include <QNetworkConfigurationManager>
#include <QList>
#include <QNetworkConfiguration>
#include <QDebug>
#include <QVariant>
#include <QNetworkSession>
#include <QNetworkInterface>
#include <QMap>
#include <QNetworkAddressEntry>
#include <QAbstractSocket>

QString getIPv4(const QNetworkConfiguration& configuration){
    QNetworkSession session(configuration);
    QNetworkInterface interface = session.interface();
    QList<QNetworkAddressEntry> addresses = interface.addressEntries();
    for(QList<QNetworkAddressEntry>::const_iterator address = addresses.begin(); address != addresses.end(); ++address){
        if(address->ip().protocol() == QAbstractSocket::IPv4Protocol){
            return address->ip().toString();
        }
    }
    return "IPv4 NOT FOUND IN THIS CONNECTION";
}

NetworkPage::NetworkPage(QWidget *parent) : QWizardPage(parent){
    setTitle(tr("Выбор интернет-подключения"));
    network_label = new QLabel(tr("Подключение:"), this);
    network_edit = new QComboBox(this);
    layout = new QGridLayout(this);
    ip_address_show = new QLineEdit(this);
    ip_address_show->setEnabled(false);
    identifier_show = new QLineEdit(this);
    identifier_show->setEnabled(false);
    QNetworkConfigurationManager manager;
    QList<QNetworkConfiguration> configurations = manager.allConfigurations(QNetworkConfiguration::Active);
    for(QList<QNetworkConfiguration>::const_iterator i = configurations.begin(); i != configurations.end(); ++i){
        network_configurations[i->name()] = *i;
        network_edit->addItem(i->name());
        if(ip_address_show->text().isEmpty()){
            ip_address_show->setText(getIPv4(*i));
            identifier_show->setText(i->identifier());
        }
    }
    layout->addWidget(network_label, 1, 1);
    layout->addWidget(network_edit, 1, 2);
    layout->addWidget(identifier_show, 2, 1);
    layout->addWidget(ip_address_show, 2 ,2);
    setLayout(layout);
    registerField("network_configuration_id", identifier_show);
    connect(network_edit, SIGNAL(currentTextChanged(QString)), this, SLOT(networkConfigurationChanged(QString)));
}

void NetworkPage::networkConfigurationChanged(QString string){
    if(network_configurations.contains(string)){
        ip_address_show->setText(getIPv4(network_configurations[string]));
        identifier_show->setText(network_configurations[string].identifier());
    }else{
        qDebug() << "Wrong network configuration";
    }
}
