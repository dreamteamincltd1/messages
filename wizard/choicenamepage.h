#ifndef CHOICENAMEPAGE_H
#define CHOICENAMEPAGE_H
#include <QWizardPage>
#include <QLabel>
#include <QTextEdit>
#include <QGridLayout>
#include <QLineEdit>

class ChoiceNamePage : public QWizardPage {
    Q_OBJECT
private:
    QLabel* name_label;
    QLabel* subname_label;
    QLineEdit* name_edit;
    QLineEdit* subname_edit;
    QGridLayout* layout;
public:
    ChoiceNamePage(QWidget* parent = 0);
};

#endif // CHOICENAMEPAGE_H
