#ifndef CONFIGWIZARD_H
#define CONFIGWIZARD_H
#include <QWizard>
#include <QWizardPage>
#include <QFile>

class ConfigWizard : public QWizard {
    Q_OBJECT
private:
    QFile* file;
public:
    ConfigWizard(QFile* file, QWidget* parent = 0);
    void accept();
    void closeEvent(QCloseEvent *);
    void reject();
};
#endif // CONFIGWIZARD_H
