#include "configwizard.h"
#include "choicenamepage.h"
#include "networkpage.h"
#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QIODevice>
#include <QUuid>
#include <QCloseEvent>

ConfigWizard::ConfigWizard(QFile* file, QWidget *parent) : QWizard(parent, Qt::WindowTitleHint | Qt::CustomizeWindowHint){
    this->file = file;
    setWindowTitle("Настройка BUTbKO");
    addPage(new ChoiceNamePage(this));
    addPage(new NetworkPage(this));
    setOption(QWizard::NoCancelButton);
}

void ConfigWizard::accept(){
    file->open(QIODevice::ReadWrite);
    QString name = field("name").toString();
    QString subname =  field("subname").toString();
    QString network_cfg_name = field("network_configuration_id").toString();
    qDebug() << network_cfg_name;
    QUuid uuid = QUuid::createUuid();
    QJsonObject config;
    config.insert("name", QJsonValue(name));
    config.insert("subname", QJsonValue(subname));
    config.insert("network_configuration_id", QJsonValue(network_cfg_name));
    config.insert("uuid", QJsonValue(uuid.toString()));
    QJsonDocument document(config);
    file->write(document.toJson());
    file->close();
    QDialog::accept();
}

void ConfigWizard::closeEvent(QCloseEvent * event){
    event->ignore();
    return;
}

void ConfigWizard::reject(){
    return;
}
