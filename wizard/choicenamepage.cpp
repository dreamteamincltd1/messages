#include "choicenamepage.h"
#include <QVBoxLayout>
#include <QDebug>
ChoiceNamePage::ChoiceNamePage(QWidget *parent) : QWizardPage(parent) {
    setTitle("Ваш профиль");
    name_label = new QLabel(tr("Имя:"), this);
    subname_label = new QLabel(tr("Фамилия:"), this);
    name_edit = new QLineEdit(this);
    subname_edit = new QLineEdit(this);
    layout = new QGridLayout(this);
    layout->addWidget(name_label, 1, 1);
    layout->addWidget(name_edit, 1, 2);
    layout->addWidget(subname_label, 2, 1);
    layout->addWidget(subname_edit, 2, 2);
    registerField("name*", name_edit);
    registerField("subname*", subname_edit);
}
