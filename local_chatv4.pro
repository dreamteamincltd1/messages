#-------------------------------------------------
#
# Project created by QtCreator 2014-10-02T10:41:29
#
#-------------------------------------------------

QT       += core gui network widgets multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = local_chatv4
TEMPLATE = app


SOURCES += main.cpp\
    core.cpp \
    userprofile.cpp \
    widget.cpp \
    listitem.cpp \
    starter.cpp \
    configuration/configurationreader.cpp \
    configuration/parameters.cpp \
    dialer/calldialog.cpp \
    dialer/callstatus.cpp \
    dialer/dialercore.cpp \
    dialer/dialermanager.cpp \
    wizard/choicenamepage.cpp \
    wizard/configwizard.cpp \
    wizard/networkpage.cpp

HEADERS  += \
    core.h \
    userprofile.h \
    widget.h \
    listitem.h \
    starter.h \
    configuration/configurationreader.h \
    configuration/parameters.h \
    dialer/calldialog.h \
    dialer/callstatus.h \
    dialer/dialercore.h \
    dialer/dialermanager.h \
    wizard/choicenamepage.h \
    wizard/configwizard.h \
    wizard/networkpage.h

FORMS    +=

RESOURCES += \
    resources.qrc
