#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QListWidget>
#include <QTextEdit>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QModelIndex>
#include <QDebug>
#include "core.h"

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget();

public slots:
    void slotButListenClicked();
    void slotButConnectClicked();
    void slotButBindClicked();
    void slotButIOnlineClicked();
    void slotButSendClicked();
    void slotListContactClicked(QModelIndex i);
    void slotNewConnection(UserProfile* prof);
    void slotDisconnectContact(UserProfile*prof);
    void slotUpdateListContact(QList<UserProfile*> list);
    void slotNewUserMessage(UserProfile *prof, QString msg);
signals:
    void signalCallTo(UserProfile* profile);
    void signalConnectToTcp(QHostAddress addr,int port);
    void signalListenTcp(QHostAddress addr,int port);
    void signalBindUdp(QHostAddress addr,int port);
    void signalSendIOnlineMessage(int port);
    void signalSendServiceMessage(QUuid uuid,QList<QPair<QString,QString> >list);
    void signalPrintUserProfileList();

private slots:
    void slotCall();

public:
    ~Widget();
private:
    QListWidget * m_listContact;
    QTextEdit * m_textIn;
    QTextEdit * m_textOut;
    QLineEdit * m_lineListenAddr;
    QLineEdit * m_lineListenPort;
    QLineEdit * m_lineConnectAddr;
    QLineEdit * m_lineConnectPort;
    QLineEdit * m_lineBindPort;
    QPushButton * m_butSendMessage;
    QPushButton * m_butListen;
    QPushButton * m_butConnect;
    QPushButton * m_butBind;
    QPushButton * m_butIOnline;
    QPushButton * m_butCall;

    //QTcpSocket * m_TcpCurrSocket;
    QUuid m_currID;
    UserProfile* m_currProfile;

    //void updateListContacts();
    void addItem(UserProfile * prof);
};

#endif // WIDGET_H
