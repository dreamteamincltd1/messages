#include "callstatus.h"

CallStatus::CallStatus() : QDialog(NULL, Qt::Window){
    m_layout = new QVBoxLayout();
    m_label = new QLabel();
    m_btn = new QPushButton("Закончить вызов");
    m_timer = new QTimer();
    m_layout->addWidget(m_label);
    m_layout->addWidget(m_btn);
    setLayout(m_layout);
    connect(m_btn, SIGNAL(clicked()), SLOT(accept()));
    connect(m_timer, SIGNAL(timeout()), SLOT(updateTime()));
}

CallStatus::~CallStatus(){
    delete m_layout;
    delete m_label;
    delete m_btn;
    delete m_timer;
}


UserProfile* CallStatus::getUserProfile(){
    return m_profile;
}

void CallStatus::setUserProfile(UserProfile * profile){
    m_profile = profile;
}


void CallStatus::setWaitingStatus(){
    m_label->setText(QString("Ожидание подтверждения от %1 %2").arg(m_profile->getName(), m_profile->getSubname()));
}

void CallStatus::startCallTimer(){
    m_time.restart();
    m_timer->start(1000);
}

void CallStatus::stopCallTimer(){
    m_timer->stop();
}

void CallStatus::updateTime(){
    int elapsed = m_time.elapsed();
    uint seconds = (elapsed / 1000) % 60;
    uint minutes = (elapsed / (1000 * 60)) % 60;
    uint hours = (elapsed / (1000 * 60 * 60)) % 60;
    m_label->setText(QString("%1:%2:%3").arg(hours, 2, 10, QChar('0'))
                                        .arg(minutes, 2, 10, QChar('0'))
                                        .arg(seconds, 2, 10, QChar('0')));
}
