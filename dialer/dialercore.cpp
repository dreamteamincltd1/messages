#include "dialercore.h"
#include <QDebug>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QMediaPlaylist>
#include <QAudioDeviceInfo>
#include <QMessageBox>
#include <QDateTime>

DialerCore::DialerCore(Parameters params) : m_parameters(params) {
    m_audio_format.setSampleRate(44000);
    m_audio_format.setByteOrder(QAudioFormat::LittleEndian);
    m_audio_format.setChannelCount(1);
    m_audio_format.setSampleSize(16);
    m_audio_format.setCodec("audio/pcm");
    m_audio_format.setSampleType(QAudioFormat::UnSignedInt);
    QAudioDeviceInfo info(QAudioDeviceInfo::defaultInputDevice());
    qDebug() << "Codecs:" << info.supportedCodecs();
    if (!info.isFormatSupported(m_audio_format))
        m_audio_format = info.nearestFormat(m_audio_format);
    m_udp_socket_in = new QUdpSocket();
    m_udp_socket_out = new QUdpSocket();
    m_audio_output = new QAudioOutput(m_audio_format);
    m_audio_input = new QAudioInput(m_audio_format);
    connect(m_udp_socket_in, SIGNAL(readyRead()), SLOT(playDatagram()));
    connect(m_audio_input, SIGNAL(stateChanged(QAudio::State)), SLOT(audioInputStateChanged(QAudio::State)));
    connect(m_udp_socket_out, SIGNAL(stateChanged(QAbstractSocket::SocketState)), SLOT(udpOutSocketStateChanged(QAbstractSocket::SocketState)));
}

DialerCore::~DialerCore(){
    delete m_udp_socket_in;
    delete m_udp_socket_out;
    delete m_audio_output;
    delete m_audio_input;
}

void DialerCore::callingFrom(UserProfile *profile){
    m_audio_output_stream = m_audio_output->start();
    if(!m_udp_socket_in->bind(QHostAddress(m_parameters.getIPv4()), 14433)){
    //if(!m_udp_socket_in->bind(QHostAddress::Any, 14433)){
        QMessageBox::warning(0, "Error", "Binding Failed");
    }

    qDebug() << "Send voice to " << profile->getSocket()->peerAddress();
    m_udp_socket_out->connectToHost(profile->getSocket()->peerAddress(), 14433);

    //m_udp_socket_out->connectToHost(QString("127.0.0.1"), 14433);
    m_audio_input->start(m_udp_socket_out);
}

void DialerCore::endCall(){
    m_audio_input->stop();
    m_audio_output->stop();
    m_udp_socket_in->close();
    m_udp_socket_out->close();
}

void DialerCore::playDatagram(){
    qDebug() << QDateTime::currentDateTime();
    while (m_udp_socket_in->hasPendingDatagrams()){
        QByteArray data;
        data.resize(m_udp_socket_in->pendingDatagramSize());
        m_udp_socket_in->readDatagram(data.data(), data.size());
        m_audio_output_stream->write(data.data(), data.size());
    }
}

void DialerCore::audioInputStateChanged(QAudio::State state){
    qDebug() << "DialerCore::changed audio input state" << state;
}

void DialerCore::udpOutSocketStateChanged(QAbstractSocket::SocketState state){
    qDebug() << "DialerCore::changed udp output state" << state;
}
