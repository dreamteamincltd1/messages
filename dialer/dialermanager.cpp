#include "dialermanager.h"
#include <QDebug>
DialerManager::DialerManager(){
    m_call_dialog = NULL;
    m_status_dialog = NULL;
    profile = NULL;

    m_active = false;
    m_player = new QMediaPlayer();
    QString location = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    QDir::current().mkpath(location);
    m_sound_file = new QFile(location + "/sound.mp3");
    if(!m_sound_file->exists())
        QFile::copy(":/call_sound", m_sound_file->fileName());
    m_playlist = new QMediaPlaylist();
    m_playlist->addMedia(QUrl(m_sound_file->fileName()));
    m_player->setPlaylist(m_playlist);
    m_playlist->setPlaybackMode(QMediaPlaylist::Loop);
    m_call_dialog = new CallDialog();
    connect(m_call_dialog, SIGNAL(accepted()), SLOT(accept()));
    connect(m_call_dialog, SIGNAL(rejected()), SLOT(reject()));
    m_status_dialog = new CallStatus();
    connect(m_status_dialog, SIGNAL(accepted()), SLOT(end()));
    connect(m_status_dialog, SIGNAL(rejected()), SLOT(end()));
}


DialerManager::~DialerManager(){
    delete m_player;
    delete m_playlist;
    delete m_sound_file;
    delete m_call_dialog;
    delete m_status_dialog;
}

void DialerManager::accept(){
    m_player->stop();
    UserProfile* profile = m_call_dialog->getProfile();
    qDebug() << "ACC call from " << profile->getSubname();
    QList<QPair<QString, QString> > args;
    args.append(QPair<QString, QString>("cmd", "call_answer"));
    args.append(QPair<QString, QString>("call", "accept"));
    emit serviceMessage(QUuid(profile->getUserID()), args);
    qDebug() << "Emit service message";
    emit startTalk(profile);
    qDebug() << "Emit start talk";
    m_status_dialog->setUserProfile(profile);
    m_status_dialog->startCallTimer();
    m_status_dialog->show();
}

void DialerManager::reject(){
    qDebug() << "REJ call from " << profile->getSubname();
    m_player->stop();
    UserProfile* profile = m_call_dialog->getProfile();
    QList<QPair<QString, QString> > args;
    args.append(QPair<QString, QString>("cmd", "call_answer"));
    args.append(QPair<QString, QString>("call", "reject"));
    emit serviceMessage(QUuid(profile->getUserID()), args);
}

void DialerManager::oCall(UserProfile *profile){
    qDebug() << "oCall";
    if(m_active){
        UserProfile* profile = m_call_dialog->getProfile();
        QList<QPair<QString, QString> > args;
        args.append(QPair<QString, QString>("cmd", "call_answer"));
        args.append(QPair<QString, QString>("call", "reject"));
        emit serviceMessage(QUuid(profile->getUserID()), args);
        return;
    }
    m_active = true;
    m_player->play();
    m_call_dialog->setProfile(profile);
    m_call_dialog->show();
}

void DialerManager::end(){
    qDebug() << "end";
    UserProfile* profile = m_status_dialog->getUserProfile();
    QList<QPair<QString, QString> > args;
    args.append(QPair<QString, QString>("cmd", "call_answer"));
    args.append(QPair<QString, QString>("call", "end"));
    emit serviceMessage(QUuid(profile->getUserID()), args);
    emit stopTalk();
    m_status_dialog->stopCallTimer();
    m_active = false;
}

void DialerManager::oAccept(UserProfile *profile){
    qDebug() << "oAccept";
    if(m_status_dialog->isVisible()){
        if(m_status_dialog->getUserProfile() == profile){
            m_status_dialog->startCallTimer();
            emit startTalk(profile);
        }
    }
}

void DialerManager::oReject(UserProfile *profile){
    qDebug() << "oReject";
    if(m_status_dialog->isVisible()){
        if(m_status_dialog->getUserProfile() == profile){
            m_status_dialog->hide();
            m_status_dialog->stopCallTimer();
            m_active = false;
            qDebug() << "Вызов сброшен другой стороной";
        }
    }
}

void DialerManager::oEnd(UserProfile *profile){
    qDebug() << "oEnd";
    if(m_status_dialog->isVisible()){
        if(m_status_dialog->getUserProfile() == profile){
            m_status_dialog->hide();
            m_status_dialog->stopCallTimer();
            m_active = false;
            emit stopTalk();
        }
    }
}

void DialerManager::call(UserProfile* profile){
    qDebug() << "call";
    m_active = true;
    QList<QPair<QString, QString> > args;
    args.append(QPair<QString, QString>("cmd", "call"));
    m_status_dialog->setUserProfile(profile);
    m_status_dialog->setWaitingStatus();
    m_status_dialog->show();
    emit serviceMessage(QUuid(profile->getUserID()), args);
}
