#ifndef DIALERMODULE_H
#define DIALERMODULE_H
#include <QObject>
#include <QAudioOutput>
#include <QAudioInput>
#include <QUdpSocket>
#include <QAudioFormat>
#include <QMediaPlayer>
#include "../configuration/parameters.h"
#include "../userprofile.h"
#include "CallDialog.h"
#include "CallStatus.h"
#include <QFile>
#include <QMediaPlaylist>
#include <QIODevice>
class DialerCore : public QObject {
    Q_OBJECT
protected:
    QIODevice* m_audio_output_stream;
    QAudioOutput* m_audio_output;
    QAudioInput* m_audio_input;
    QUdpSocket* m_udp_socket_out;
    QUdpSocket* m_udp_socket_in;
    Parameters m_parameters;
    QAudioFormat m_audio_format;
public:
    DialerCore(Parameters parameters);
    virtual ~DialerCore();
public slots:
    void callingFrom(UserProfile* profile);
    void endCall();
private slots:
    void playDatagram();
    void audioInputStateChanged(QAudio::State);
    void udpOutSocketStateChanged(QAbstractSocket::SocketState);
};
#endif // DIALERMODULE_H
