#ifndef DIALERMANAGER_H
#define DIALERMANAGER_H
#include <QObject>
#include <QFile>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include "CallDialog.h"
#include "CallStatus.h"
#include "../userprofile.h"
#include <QList>
#include <QPair>
#include <QUuid>
#include <QStandardPaths>
#include <QDir>
class DialerManager : public QObject {
    Q_OBJECT
protected:
    bool m_active;
    QMediaPlayer* m_player;
    QMediaPlaylist* m_playlist;
    QFile* m_sound_file;
    CallDialog* m_call_dialog;
    CallStatus* m_status_dialog;
    UserProfile* profile;
public:
    DialerManager();
    virtual ~DialerManager();
public slots:
    void end();
    void accept();
    void oCall(UserProfile* profile);
    void reject();
    void oEnd(UserProfile* profile);
    void oAccept(UserProfile* profile);
    void oReject(UserProfile* profile);
    void call(UserProfile* profile);
signals:
    void startTalk(UserProfile* profile);
    void stopTalk();
    void serviceMessage(QUuid profile, QList<QPair<QString, QString> > args);
};

#endif // DIALERMANAGER_H
