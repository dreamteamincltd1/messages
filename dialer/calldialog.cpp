#include "calldialog.h"
#include <QDebug>
#include <QIcon>
#include <QFile>
#include <QApplication>
CallDialog::CallDialog() : QDialog(NULL, Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint){
    m_layout = new QVBoxLayout();
    m_h_layout = new QHBoxLayout();
    m_accept = new QPushButton(QIcon(":/call_icon"), "Accept");
    m_reject = new QPushButton(QIcon(":/call_reject_icon"), "Reject");
    m_label = new QLabel("");
    m_layout->addWidget(m_label);
    m_layout->addLayout(m_h_layout);
    m_h_layout->addWidget(m_accept);
    m_h_layout->addWidget(m_reject);
    setLayout(m_layout);
    connect(m_accept, SIGNAL(clicked()), SLOT(accept()));
    connect(m_reject, SIGNAL(clicked()), SLOT(reject()));
}

CallDialog::~CallDialog(){
    delete m_label;
    delete m_accept;
    delete m_reject;
    delete m_h_layout;
    delete m_layout;
}

void CallDialog::setProfile(UserProfile *profile){
    m_profile = profile;
    m_label->setText(QString("Входящий звонок от %1 %2").arg(profile->getName(), profile->getSubname()));
}

UserProfile* CallDialog::getProfile(){
    return m_profile;
}
