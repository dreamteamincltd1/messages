#ifndef CALLSTATUS_H
#define CALLSTATUS_H
#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>
#include <QTimer>
#include <QPushButton>
#include <QTime>
#include "../userprofile.h"
class CallStatus : public QDialog {
    Q_OBJECT
protected:
    QVBoxLayout* m_layout;
    QLabel* m_label;
    QPushButton* m_btn;
    QTimer* m_timer;
    QTime m_time;
    UserProfile* m_profile;
public:
    CallStatus();
    virtual ~CallStatus();
    void setUserProfile(UserProfile*);
    UserProfile* getUserProfile();
    void setWaitingStatus();
    void startCallTimer();
    void stopCallTimer();
private slots:
    void updateTime();
signals:
    void endCall();
};

#endif // CALLSTATUS_H
