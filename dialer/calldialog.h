#ifndef CALLDIALOG_H
#define CALLDIALOG_H
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include "../userprofile.h"
class CallDialog : public QDialog{
    Q_OBJECT
protected:
    QVBoxLayout* m_layout;
    QHBoxLayout* m_h_layout;
    QPushButton* m_accept;
    QPushButton* m_reject;
    QLabel* m_label;
    UserProfile* m_profile;
public:
    CallDialog();
    virtual ~CallDialog();
    void setProfile(UserProfile* profile);
    UserProfile* getProfile();
};

#endif // CALLDIALOG_H
